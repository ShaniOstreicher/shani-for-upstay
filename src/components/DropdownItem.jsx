import React, { useState } from "react";

function DropdownItem({ item, deep }) {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <div
      className="dropdown-item-container"
      key={item.title}
      onMouseEnter={() => {
        setIsOpen(true);
      }}
      onMouseLeave={() => {
        setIsOpen(false);
      }}
    >
      <div className="dropdown-item-title">{item.title}</div>
      {isOpen && (
        <div
          className="dropdown-item"
          style={
            deep === 0
              ? { top: "20px", left: 0 }
              : { top: "-10px", left: "130px" }
          }
        >
          {item.submenus?.map((item) => {
            return (
              <DropdownItem key={item.title} item={item} deep={deep + 1} />
            );
          })}
        </div>
      )}
    </div>
  );
}

export default DropdownItem;
