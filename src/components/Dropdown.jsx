import React from "react";
import DropdownItem from "./DropdownItem";

const DATA = [
  {
    title: "Sub Menu 1",
    submenus: [
      {
        title: "Sub deep Menu 1",
        submenus: [{ title: "Sub veryyy deep Menu 1" }],
      },
    ],
  },
  {
    title: "Sub Menu 2",
    submenus: [
      {
        title: "Sub deep Menu 1",
        submenus: [{ title: "Sub veryyy deep Menu 1" }],
      },
      {
        title: "Sub deep Menu 2",
        submenus: [{ title: "Sub veryyy deep Menu 1" }],
      },
    ],
  },
  {
    title: "Sub Menu 3",
    submenus: [
      {
        title: "Sub deep Menu 1",
        submenus: [
          {
            title: "Sub veryyy deep Menu 1",
            submenus: [{ title: "Sub veryyy veryyy veryyy deep Menu 1" }],
          },
        ],
      },
      {
        title: "Sub deep Menu 2",
        submenus: [{ title: "Sub veryyy deep Menu 1" }],
      },
      {
        title: "Sub deep Menu 3",
        submenus: [{ title: "Sub veryyy deep Menu 1" }],
      },
    ],
  },
];

function Dropdown(props) {
  return (
    <ul className="dropdown">
      {DATA.map((item) => (
        <DropdownItem key={item.title} item={item} deep={0} />
      ))}
    </ul>
  );
}

export default Dropdown;
