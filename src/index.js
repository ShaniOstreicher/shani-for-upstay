import React from "react";
import ReactDOM from "react-dom";

import "./style.css";
import Dropdown from "./components/Dropdown";

function App() {
  return (
    <div className="App">
      <Dropdown></Dropdown>
    </div>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
